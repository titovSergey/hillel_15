Домашнее задание 17
Добавлено: 12.02.2020 21:01
Проработка урока №15
Исходный файл в группе Telegram.

15.zip

В конечном итоге предоставить доступ к репозиторию или загрузить код:


1.Вынести вывод из контроллера в файл index.php
2.Реализовать активность в панели навигации по сайту
3.Передать передачу данных для навигации из файла blade.php
4.Создать модель Navigation
5.Реализовать файлы создания и заполнения, удаления таблицы navigations
6.Воспользоваться моделью и таблицей для передачи пунктов навигации в шаблон layout


Все вышеупомянутые пункты реализованы на уроке. Для их выполнения необходимо взять
стартовый архив и проработать видео урока.



config/blade.php

$blade->composer('layouts.layout', function ($view){
//    var_dump(\App\Model\Navigation::all());
//    exit;

        $view->with('nav', \App\Model\Navigation::all());

//        $view->with('nav', [
//       [
//           'href' => '/',
//           'name' => 'home',
//           'title' => 'Home',
//       ],
//       [
//           'href' => '/blog',
//           'name' => 'blog',
//           'title' => 'Blog',
//       ],
//       [
//           'href' => '/services',
//           'name' => 'services',
//           'title' => 'Services',
//       ],
//       [
//           'href' => '/team',
//           'name' => 'team',
//           'title' => 'Team',
//       ],
//       [
//           'href' => '/contact-us',
//           'name' => 'contacts',
//           'title' => 'Contacts',
//       ]
//   ]);
//});


//$blade->composer('layouts.layout', function ($view){
//    $view->with('nav', \App\Model\Navigation::all());
//});

